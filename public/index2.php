<?php
/**
 * Интернет-программирование. Задача 9.
 * Реализовать скрипт на веб-сервере на PHP или другом языке,
 * сохраняющий в XML-файл заполненную форму задания 7. При
 * отправке формы на сервере создается новый файл с уникальным именем.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
           
    // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
    if (!empty($_COOKIE['PHPSESSID'])){ //ЗНАЧИТ пользователь
        //вошёл в систему, нужно загрузить данные из бд
        printf("You entred</br>");
    }
    if (!empty($_COOKIE['save']) && empty($_COOKIE['elogin'])) {
        
        printf('Your password is : ');
        echo sprintf($_COOKIE['passw']);
        printf('<br>Your login is: ');
        echo sprintf($_COOKIE['login']);
        
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', 0, 1);
        setcookie('login', 0, 1);
        setcookie('passw', 0, 1);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Thank you, date is  save!';
       printf('<br>Data is saved');
     }
    
     
    
}
else { //Если отправка методом POST
        $user = 'u20341';
        $pass = '3227715';
        $db = new PDO('mysql:host=localhost;dbname=u20341', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
        //Сохранение в базу данных
    if (empty($_COOKIE['PHPSESSID'])){ //пользователь не авторизован
        $passw = substr(md5(time()), 0, 7);
       $login = substr(md5(time()), 0, 5);
        setcookie('passw',$passw);
        setcookie('login', $login);
    
         try{
                $fio = $_POST['fio'];
                $mail = $_POST['mail'];
                $birthday = $_POST['date1'];
                                               
                if ($_POST['radio_group_1'] === 'man'){
                    $gender = 'man';
                }
                else{
                    $gender = 'woman';
                }
                if ($_POST['radio-group_2']==='1'){
                    $legs = 1;
                } else if ($_POST['radio-group_2']==='2'){
                    $legs = 2;
                } else if ($_POST['radio-group_2']==='3'){
                    $legs = 3;
                }
                if ($_POST['herous'] === 'fly'){
                    $fly = '1';
                }
                else $fly = '0';
                if ($_POST['herous'] === 'idclip'){
                    $idclip = "1";
                } else $idclip = '0';
                if ($_POST['herous'] === 'deathless'){
                    $deathless = "1";
                }else $deathless = '0';
                if ($fly!="1" && $idclip != "1" && $deathless != "1"){
                    setcookie('error_herous', '1', time()+24*60*60);
                    $errors = TRUE;
                }
                else{
                    setcookie('idclip', $idclip, time()+30*24*60*60);
                    setcookie('deathless', $deathless, time()+30*24*60*60);
                    setcookie('fly', $fly, time()+30*24*60*60);
                }
                if ($_POST['check1'] === '1'){
                    $a = '1';
                } else $a=0;
        //Проверяем правильность заполнения полей
        $errors = FALSE;
if (empty($_POST['fio'])) {
    setcookie('error_fio', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('fio', $_POST['fio'], time()+30*24*60*60);
}
if (empty($_POST['mail'])) {
    setcookie('error_mail', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('mail', $_POST['mail'], time()+30*24*60*60);
}
if (empty($_POST['date1'])) {
    setcookie('error_date1', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    if(!preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|
([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $_POST['mail'])){
setcookie('error_mail', '2', time()+24*60*60);
$errors = TRUE;
    }
    setcookie('date1', $_POST['date1'], time()+30*24*60*60);
}
if (empty($_POST['radio_group_1'])) {
    setcookie('error_r_g_1', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('r_g_1', $_POST['radio_group_1'], time()+30*24*60*60);
}
if (empty($_POST['radio-group_2'])) {
    setcookie('error_r_g_2', '1', time()+24*60*60);;
    $errors = TRUE;
}
else{
    setcookie('r_g_2', $_POST['radio-group_2'], time()+30*24*60*60);
}
if (empty($_POST['biography'])){
    setcookie('error_biogr', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('biogr', $_POST['biography'], time()+30*24*60*60);
}
if ($_POST['agree']==='0'){
    setcookie('error_agree', '1', time()+24*60*60);
    $errors = TRUE;
}
else{
    setcookie('agree', $_POST['agree'], time()+30*24*60*60);
}

            // Подготовленный запрос. Не именованные метки.
            
                    $stmt = $db->prepare("INSERT INTO task_5 SET name = ?, LOGIN = ?, PASSWORD = ?, email = ?, birthday = ?, gender = ?, legs = ? 
                ,deathless = ?, idclip = ?, fly = ?, biography = ?, agreement = ?");
                    $stmt -> execute(array($fio, $login, $passw, $mail, $birthday, $gender, $legs, $deathless, $idclip, $fly,
                       $_POST['biography'], $a));
        }
        
        catch(PDOException $e){
            print('Error : ' . $e->getMessage());
            exit();
        }
    }
    if (!empty($_COOKIE['PHPSISSID'])) { //если пользователь зашел не в первый раз
        //загрузить в форму значения из бд
          $stmt = $db->prepare("SELECT * FROM task_5 WHERE LOGIN = ?");
          $stmt->execute(array($l));
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          
       setcookie('not first', 1);
      
       
            
// TODO: перезаписать данные в БД новыми данными,

// Подготовленный запрос. Не именованные метки.
    $stmt = $db->prepare("UPDATE task_5 SET name = ?, email = ?, birthday = ?, gender = ?, legs = ?, 
    deathless = ?, idclip = ?, fly = ?, biography = ?, agreement = ? WHERE LOGIN = ?");
        $stmt -> execute(array($_POST['name'], $_POST['mail'], $_POST['date1'], $gender, $legs, $deathless, $idclip, $fly,
            $_POST['biography'], $a, $l ) );
    }
    setcookie('save', '1');
    header('Location: index2.php');
   }
